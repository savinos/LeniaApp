Δεδομένου(/^ότι βρίσκομαι στ(?:ο|ην) "([^\"]*)"$/) do |page_name|
  visit path_to(page_name)
end

Τότε(/^(?:θέλω να|ότι) βλέπω στη "([^\"]*)" το "([^\"]*)"$/) do |position, object|
  case position
  when /^Κορυφή$/
    position = 'header'
  else
    pending "#{position} τί είναι;"
  end

  case object
  when /^Μενού Πλοήγησης$/
    object = 'nav'
  else
    pending "#{object} τί είναι;"
  end

  expect(find(position)).to have_xpath(object)
end

Τότε(/^θέλω στο "([^\"]*)" να βλέπω σύνδεσμο με τίτλο (.*)$/) do |object, title|
  case object
  when /^Μενού Πλοήγησης$/
    object = 'nav'
  else
    pending "#{object} τί είναι;"
  end

  expect(find(object)).to have_link(title)
end

Τότε(/^θέλω να βλέπω στο περιεχόμενο της Σελίδας μια φωτογραφία και ένα ρητό$/) do
  expect(page).to have_selector('img')
  expect(page).to have_selector('blockquote')
end
