#language: el
Λειτουργία: Home Page
  Το Home Page της εφαρμογής
  Θέλω να είναι ελκυστικό ως προς το χρήστη
  και να παρέχει τα απαραίτητα όπως
  μενού πλοήγησης και περιγραφή της εφαρμογής.

  Υπόβαθρο:
    Δεδομένου ότι βρίσκομαι στο "Home Page"
    
  Σενάριο: Μενού Πλοήγησης
    Τότε θέλω να βλέπω στη "Κορυφή" το "Μενού Πλοήγησης"
    
  Περιγραφή Σεναρίου: Συνδέσμοι στο Μενού Πλοήγησης
  Τότε θέλω στο "Μενού Πλοήγησης" να βλέπω σύνδεσμο με τίτλο <Τίτλος Συνδέσμου>
    
    Παραδείγματα:
    | Τίτλος Συνδέσμου    |
    | Ανώγυρο             |
    | Προϊόντα            |
    | Σοδιά               |
    | Προϊόντα ανά Χρονιά |

  Σενάριο: Φωτογραφία και Ρητό
    Για να είναι πιο ελκυστική η εφαρμογή
    θέλω στη πρώτη σελίδα να εμφανίζεται
    μια φωτογραφία και ένα ρητό
    # Μπορεί να γίνει και τυχαία
    Τότε θέλω να βλέπω στο περιεχόμενο της Σελίδας μια φωτογραφία και ένα ρητό



